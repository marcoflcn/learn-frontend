# Lessons

Here you can find the list of topics for each lesson.

## 2021-04-18

- what is an API
- auth flow example
- Promise object
- fetch data from Rick&Morty API and show characters

### Homeworks

- Look homeworks for 2021-03-21 (guess a number)
- Add load more button to rick.html

## 2021-04-11

- what is software versioning (folder snapshot)
- what is git
- how to create a version with git (commit)
- how to start from existing project (clone)
- how to interact with a remote git repository (project) for save (push) and read (pull)

### Homeworks

Look homeworks for 2021-03-21 (guess a number)

## 2021-03-28

- recap on events
- recap on form
- discuss accessibility

### Homeworks

Look homeworks for 2021-03-21

## 2021-03-21

- html tables
- html forms
- submit event for forms node
- read form's inputs values
- write results in DOM node

### Homeworks

Convert guess_a_number exercise removing `alert` and `prompt` and using a form as input and a DOM node as output in page.
As additional challenge you can try to write all numbers guessed.

## 2021-03-14

- dev console and console.log
- read DOM nodes
- edit node styles
- edit node class list [https://developer.mozilla.org/en/docs/Web/API/Element/classList](https://developer.mozilla.org/en/docs/Web/API/Element/classList)
- events system

## 2021-03-07

- What is an object
- How to mix data types (array of objects and object parameters)
- Start interacting with the DOM

## 2021-02-28

- What is an array, how to create it and how to access to each element (index starts from 0)
- For loop as an alternative to while especially for sequences
- Multidimensional array also known as array of array (see list_more_complicated.js)

### Notes

You can test different scripts changing the path of script tag in index.html. Example:

```html
<script src="script/list_more_complicated.js"></script>
```

can become

```html
<script src="script/fibonacci.js"></script>
```

### Homeworks

Calculate the nth element of Fibonacci sequence. For more details look at fibonacci.js file.

## 2021-02-21

- What is a function and why helps to write code
- Variables scope and how to mask them

### Homeworks

Modify min and max values on each guess in order to give to user e feedback on the number interval.

## 2021-02-14

- Split the page elements in blocks
- Define space dynamically with `flexbox`
- A use case for while loop

## 2021-02-07

- The **comparison operators** (`>` , `<` , `==`) on numbers and strings
- `null` and `undefined` values
- `if` for conditional operations
- `while` for repeat operations
