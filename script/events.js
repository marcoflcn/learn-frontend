/**
 * document
 * |- head
 *    | ...
 * |- body
 *    |- h1
 *    |- p
 *    |- button {id: "btn-1"}
 *    |- button {id: "btn-2"}
 *    |- script {src: "script/constructor.js"}
 */
// document is the object that allow us to access DOM map

// Data
// Type: Number, String, Array, Object, Function
var variable_number = 1; // Integer
var variable_number_2 = 1.1; // Float number
var variable_string = "string";
var variable_array = [1, 2, 3, 4];
var variable_array_2 = [1, "string", 3.0, 4];
var variable_object = {
  a: 1,
  b: 2,
};
// variable_object.a == 1
var variable_function = function () {
  // Anonymous function
  alert("Function");
};
// variable_function();

// Function name: hello
// Function parameters:
// 1. name
function greeting(name) {
  alert("Hi " + name);
}

// greeting("Marco");
// var name = "Marco"
// alert("Hi " + name); -> alert("Hi " + "Marco"); -> alert("Hi Marco");

function operation(a, b, op) {
  op(a, b);
}

operation(1, 1, function (a, b) {
  return a + b;
});

function add(a, b) {
  return a + b;
}

// alert(add(1, 2));
// alert(add(2, 3));

add(1, 2);

var btn_1 = document.getElementById("btn-1");
var btn_2 = document.getElementById("btn-2");
btn_1.addEventListener("click", function (event) {
  alert("btn-1");
});
btn_1.addEventListener("click", function (event) {
  alert("Second listener");
});

btn_2.addEventListener("click", function (event) {
  alert("btn-2");
});

var increment_button = document.getElementById("inc");
var text_box = document.getElementById("text-box");
var counter = 0;

increment_button.addEventListener("click", function (event) {
  counter = counter + 1;
  text_box.textContent = "Ho cliccato " + counter + " volte";
});

// Form
var form = document.getElementById("form");
var form_output = document.getElementById("form-result");
var input = document.getElementById("input");
form.addEventListener("submit", function (event) {
  event.preventDefault();
  form_output.textContent =
    "L'utente ha inserito il seguente valore: " + input.value;
});
