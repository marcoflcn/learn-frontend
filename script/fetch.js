// API - Application Program Interface
// JSON - JavaScript Object Notation
// JWT - Json Web Token

// HTTP requests
// - url / path / endpoint  https://mysite.com/path/to/resource
// - method  GET | POST | PUT | PATCH | DELETE | HEAD
// - body  Request content
// - headers Request metadata

// Comment one line
/* Comment multiple lines */
/*
 Comment multiple lines
*/

/**
 * Auth
 *
 * # Request
 * POST https://mysite.com/auth
 * Content-Type: application/json
 *
 * {
 *   "user": "alemale",
 *   "pass": "1234"
 * }
 *
 * # Response
 * Content-Type: application/json
 *
 * {
 *   "status": "success",
 *   "data": {
 *     "token": "asdasdasdasdsadasdsadsas"
 *   }
 * }
 *
 * # Request 2
 * GET https://mysite.com/my/resource
 * Authorization: Bearer asdasdasdasdsadasdsadsas
 */

/**
 * This function create an HTML box containing given character info.
 *
 * @param array character
 * @returns HTMLNode
 */
function create_character_box(character) {
  // <article>
  //   <h2>Name</h2>
  //   <p>Status</p>
  //   <img src="https://rickandmortyapi.com/api/character/avatar/1.jpeg" />
  // </article>;
  var box = document.createElement("article"),
    char_name = document.createElement("h2"),
    char_status = document.createElement("p"),
    char_photo = document.createElement("img");

  char_name.textContent = character.name;
  char_status.textContent = character.status;
  char_photo.setAttribute("src", character.image);

  box.append(char_name, char_status, char_photo);
  return box;
}
// Spread operator
// var list = [1, 2, 3, 4];
// function fn(a, b, c, d) {}
// fn(...list); -> fn(list[0], list[1], list[2], list[3])
function create_boxes(characters) {
  document.body.append(...characters.map(create_character_box));
}

var current_page = 1;
function fetch_characters() {
  fetch("https://rickandmortyapi.com/api/character?page=" + current_page)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      create_boxes(data.results);
    })
    .catch(function (error) {
      console.log(error);
    });
}

fetch_characters();
