// POSITIONS (n): 1  2  3  4  5  6  7  8 ...
// VALUES:        1  1  2  3  5  8 13 21 ...
// (1 + 1) -> 2 3 5 8
// 1 (1 + 2) -> 3 5 8
// 1 1 (2 + 3) -> 5 8
// 1 1 2 (3 + 5) -> 8
// fibonacci(n) = fibonacci(n - 1) + fibonacci(n - 2)
// fibonacci(1) = 1
// fibonacci(2) = 1

function fibonacci(n) {
  var value;
  if (n == 1 || n == 2) {
    value = 1;
  } else {
    var pre_1 = 1,
      pre_2 = 1;
    for (var i = 3; i < n; i++) {
      var temp = pre_1;
      pre_1 = pre_2;
      pre_2 = pre_1 + temp;
    }
    value = pre_1 + pre_2;
  }
  return value;
}

function sum(n) {
  var sum = 0;
  for (i = 1; i <= n; i++) {
    sum += i;
  }
  return sum;
}

// This event ensure that all DOM is loaded
// It's a good practice so if we read DOM elements we are sure that exists
window.addEventListener(
  "load",
  // Here you have a different syntax (way to write) an anonymous function
  // (event) => {} is equivalent to function(event) {}
  function (event) {
    var formNode = document.getElementById("fibonacci_form"),
      resultNode = document.getElementById("fibonacci_result");

    formNode.addEventListener("submit", function (event) {
      event.preventDefault();
      // Javascript object containing data inserted in form
      var data = new FormData(formNode),
        // Get input value by its name attribute value
        position = parseInt(data.get("position"));
      // Input checks
      console.log(position);
      if (!Number.isInteger(position)) {
        // Is not integer
        resultNode.textContent = "Position must be a number";
      } else if (position <= 0) {
        // Is zero or negative
        resultNode.textContent = "Position must be a positive integer";
      } else {
        // Is valid
        fibonacciValue = fibonacci(position);
        resultNode.textContent = `Fibonacci of ${position} is equal to ${fibonacciValue}`;
      }
    });
  }
);
