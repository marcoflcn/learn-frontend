var max = 100;
var min = 1;

function validate(number_to_validate, number_to_guess) {
  // Check if a value is provided
  if (number_to_validate == null) {
    alert("You have to provide an input");
  } else if (number_to_validate == "") {
    // Check if at least one character is inserted
    alert("This is not a valid number");
  } else if (number_to_validate > max) {
    alert("Number greater than max");
  } else if (number_to_validate < min) {
    alert("Number lower than min");
  } else if (number_to_validate > number_to_guess) {
    // Number is too great
    max = number_to_validate - 1;
    alert("Number is lower than " + number_to_validate);
  } else if (number_to_validate < number_to_guess) {
    // Number is too low
    min = parseInt(number_to_validate) + 1;
    alert("Number is greater than " + number_to_validate);
  }
  // If I guess 1 and is too low min should become 2
  number = prompt("Nope!\nInsert your guess between " + min + " and " + max);
}

// Generate a random number between min and max - 1
// 0 <= x < 1
var toGuess = Math.floor(Math.random() * max) + min;
alert("DEBUG - This is the number user should guess:" + toGuess);
var number;

// Ask user for a number
number = prompt("Insert your guess between " + min + " and " + max);

// Check if number match
// While input is not equal to toGuess
while (number != toGuess) {
  validate(number, toGuess);
}

alert("HURRAY!");
