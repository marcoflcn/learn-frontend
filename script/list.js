// Array
var arr = [1, 2, 3, 4];
// An example is an agenda
var agenda = ["Marco", "Martina", "Luigi", "Vincenzina"];
// i++ -> i = i + 1;
for (var i = 0; i < agenda.length; i++) {
  // How to access array elements
  // variable_name[index_value]
  // Examples:
  // agenda[0] == "Marco"
  // agenda[2] == "Luigi"
  alert(agenda[i]);
}
