// Pair key/value
// var person = {
//   name: "Marco",
//   country: "LU",
// };

// person.name == "Marco"
// person.country == "LU"

// var agenda = [
//   {
//     name: "Marco",
//     country: "LU",
//     age: 31,
//     job: "Developer",
//     phone: "XXXXXXXX",
//     email: "XXXXX@XXXX.XXX",
//   },
//   {
//     name: "Martina",
//     country: "LU",
//     age: 31,
//     job: "Developer",
//     phone: "XXXXXXXX",
//     email: "XXXXX@XXXX.XXX",
//   },
//   {
//     name: "Luigi",
//     country: "UK",
//     age: 31,
//     job: "Developer",
//     phone: "XXXXXXXX",
//     email: "XXXXX@XXXX.XXX",
//   },
//   {
//     name: "Vincenzina",
//     country: "IT",
//     age: 31,
//     job: "Developer",
//     phone: "XXXXXXXX",
//     email: "XXXXX@XXXX.XXX",
//   },
// ];
// for (var i = 0; i < agenda.length; i++) {
//   var name = agenda[i].name;
//   var country = agenda[i].country;
//   alert(name + " lives in " + country);
// }

var person = {
  name: "Marco",
  country: "LU",
};

var animal = {
  name: "fuffy",
  breed: "chiuahah",
  owner: person,
  vaccination: ["2020-02-28", "2021-02-28"],
  bark: function () {
    alert("BAU!");
  },
};

alert(animal.owner.name); // Marco

animal.bark(); // alert("BAU!");
