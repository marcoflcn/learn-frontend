// Array
var arr = [1, 2, 3, 4];
// An example is an agenda
var agenda = [
  ["Marco", "LU"],
  ["Martina", "LU"],
  ["Luigi", "UK"],
  ["Vincenzina", "IT"],
];
// i++ -> i = i + 1;
// i-- -> i = i - 1;
for (var i = 0; i < agenda.length; i++) {
  // How to access array elements
  // variable_name[index_value]
  // Examples:
  // agenda[0] == "Marco"
  // agenda[2] == "Luigi"

  // Access person name:
  //   var person = agenda[i];
  //   var name = person[0];
  // is equal to:
  var name = agenda[i][0];
  // Access person name:
  //   var person = agenda[i];
  //   var country = person[1];
  // is equal to:
  var country = agenda[i][1];
  alert(name + " lives in " + country);
}
