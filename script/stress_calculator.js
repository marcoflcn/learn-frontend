// Values are from 1 to 10
// var stress_list = [5, 3, 3, 9, 6];
// var satisfaction_list = [5, 4, 4, 3, 10];

function calc_stress(stress, satisfaction) {
  var result;
  if (stress.length !== satisfaction.length) {
    result = null;
  } else {
    // Calcoliamo se la somma di soddisfazione é maggiore della somma di stress
    var stress_sum = 0;
    var satisfaction_sum = 0;
    for (var i = 0; i < stress.length; i++) {
      stress_sum = stress_sum + stress[i];
      satisfaction_sum = satisfaction_sum + satisfaction[i];
    }
    result = stress_sum > satisfaction_sum; // true or false
  }
  return result;
}

document
  .getElementById("stress_form")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    var stress_list = [
      document.getElementById("mon_stress").value,
      document.getElementById("tue_stress").value,
      document.getElementById("wed_stress").value,
      document.getElementById("thu_stress").value,
      document.getElementById("fri_stress").value,
    ];
    var satisfaction_list = [
      document.getElementById("mon_satisfaction").value,
      document.getElementById("tue_satisfaction").value,
      document.getElementById("wed_satisfaction").value,
      document.getElementById("thu_satisfaction").value,
      document.getElementById("fri_satisfaction").value,
    ];
    var result = calc_stress(stress_list, satisfaction_list);
    if (result) {
      document.getElementById("result").textContent = "Licenziati pazza!!!";
    } else {
      document.getElementById("result").textContent = "Vabbé puoi continuare";
    }
  });
