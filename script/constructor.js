function Person(first_name, last_name, age) {
  this.first_name = first_name;
  this.last_name = last_name;
  this.age = age;
  this.full_name = this.first_name + " " + this.last_name;
  this.can_drink = this.age >= 21;

  if (this.age >= 21) {
    this.disclaimer = "Bevi con moderazione";
  } else {
    this.disclaimer = "Bere nuoce gravemente ai tuoi neuroni";
  }

  this.yell = function () {
    alert("Mi chiamo " + this.full_name);
  };
}

var person_1 = new Person("Marco", "Falcone", 31);
var person_2 = new Person("Bimbo", "Bello", 1);

console.log(person_1);
console.log(person_2);

person_2.yell();

// Evento "mi scotto"
// Action function mettiDentifricio() {
//     vaiInBagno();
//     apriTubetto();
//     ...
// }

// document.addEventListener("mi scotto", "mettiDentifricio");
// document.addEventListener("mi scotto", function() {
//     vaiInBagno();
//     apriTubetto();
//     ...
// });

// document.addEventListener("click", function() {
//     ...
// });
