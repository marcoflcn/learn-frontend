# Learn Frontend

Welcome Ale!

This is the place where our code will be stored.

Don't worry, you will simply download a .zip file like from the email.
To do so you have to click on the download icon (see image below).

![How to download](images/how_to_download.png)

The only difference is that I'll upload new contents here.

## Lessons

I decided to resume our lessons **[at this link](LESSONS.md)**, so you can easily review them.
